import threading
import queue
import network
import configparser

def handle(connection):
	while True:
		data = connection.recvall().strip()
		if data:
			connection.server.queue.put(data)

def startServer(q):
	host = config.get('host', 'IPv4')
	port = config.get('host', 'port')
	server = network.ThreadedTCPServer((host, int(port)), network.TCPHandler)
	server.customHandler = handle
	server.queue = q
	w = threading.Thread(target=server.serve_forever)
	w.setDaemon(True)
	w.start()

def server():
	'''
	use server
	'''
	dfcc_queue_orders = queue.Queue()
	startServer(dfcc_queue_orders)

	while True:
		message = dfcc_queue_orders.get()
		print('received: {}'.format(message))

def client():
	'''
	use client
	'''
	with network.Client('localhost', 4243) as c:
		c.send('Hello World')

if __name__ == '__main__':
	configurationFile = 'network.ini'
	config = configparser.ConfigParser()
	config.read(configurationFile)
	server()
