import socket
import socketserver
import time

class Client(object):
	def __init__(self, addr, port):
		self.addr = addr
		self.port = port
		self.socket = socket.socket()
		self.socket.connect((self.addr, self.port))

	def close(self):
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()

	def __enter__(self):
		return self
				
	def __exit__(self, type, value, traceback):
		self.close()

	def send(self, msg):
		self.socket.send(msg.encode())

class TCPHandler(socketserver.BaseRequestHandler):
	"""
	handler for network for the daemon
	"""

	def sendall(self, message):
		self.request.sendall(message.encode())

	def recvall(self, timeout=0.2):
		self.request.setblocking(0)
		total_data = []
		data = ''
		begin = time.time()
		while True:
			if total_data and time.time() - begin > timeout:
				break
			elif time.time() - begin > timeout * 2:
				break
			try:
				data = self.request.recv(2048).decode()
				if data:
					total_data.append(data)
					begin = time.time()
				else:
					time.sleep(0.1)
			except:
				pass
		return str.join('', total_data)

	def handle(self):
		return self.server.customHandler(self)

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
	allow_reuse_address = True
